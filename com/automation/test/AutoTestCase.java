package com.automation.test;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AutoTestCase {
	public WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", "/Users/nagraj/Desktop/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@Test(priority = 1, description = "Land on Login  Page")
	public void navigateToSignInPage() {
		driver.get("https://www.amazon.in/your-account");
	}

	@Test(priority = 2, description = "Click On SignIn Option")
	public void clickSignInOption() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,300)");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//span[@class='action-inner']")).click();
	}

	@Test(priority = 3, description = "Pass EmailId And click Contnue")
	public void passEmailIdAndClickContnue() throws InterruptedException {
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("amazon_username");
		driver.findElement(By.xpath("//input[@id='continue']")).click();
	}

	@Test(priority = 4, description = "Enter Password And Click on Submit")
	public void enterPasssworddAndClickSubmit() throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='ap_password']")).sendKeys("amazon_password");
		driver.findElement(By.xpath("//input[@id='signInSubmit']")).click();
	}

	@Test(priority = 5, description = "Search And Click option")
	public void searchAndClickOption() throws InterruptedException {
		driver.findElement(By.xpath("//input[@name='field-keywords']")).sendKeys("Redmi Note");
		driver.findElement(By.xpath("//input[@value='Go']")).click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
		driver.findElement(By.xpath("//h2[contains(text(),'Redmi Y2 (Rose Gold, 3GB RAM, 32GB Storage)')]")).click();
	}

	@Test(priority = 6, description = "Search And Click option")
	public void addToCard() throws InterruptedException {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.findElement(By
				.xpath("//div[@id='buyBackAccordionRow']//a[@class='a-accordion-row a-declarative accordion-header']"))
				.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[contains(text(),'Without Exchange')]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='add-to-cart-button']")).click();
		Thread.sleep(2000);

	}

	@Test(priority = 7, description = "Search And Click option")
	public void verifyAddedList() throws InterruptedException {
		WebElement element = driver.findElement(By.xpath("//a[@id='hlb-ptc-btn-native']"));
		if (element.isDisplayed() && element.isEnabled()) {
			System.out.println("Test Pass");
		} else {
			System.out.println("Test Fail");
		}
		driver.findElement(By.xpath("//a[@id='hlb-ptc-btn-native']")).click();
	}

	@AfterClass
	public void afterMethod() {
		driver.quit();
	}
}
